<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	<title>Home</title>
		<meta http-equiv="Content-Type" content="text/html; charset= ISO-8859-1" />
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<script type="text/javascript" src="script.js"> </script>
</head>
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<body onclick="mostrarBuscador('buscador',0)">

<?php  	
	//Inicializamos todas las variables session. 
	session_start();
	if (!isset($_SESSION["idUser"])){
		$_SESSION["idUser"] = 'Anonymous';
	}	
	if (!isset($_SESSION["admin"])){
		$_SESSION["admin"] = '0';
	}		
	if (!isset($_SESSION["conectado"])){
		$_SESSION["conectado"] = 'false';
	}
	
	if (!isset($_POST['login'])){
		$log = '';
	}else{
		$log = $_POST['login'];
	}	
	
	if (!isset($_POST['pass'])){
		$pass = '';
	}else{
		$pass = $_POST['pass'];
	}

	//Solo entrar� en el momento que se intent� iniciar una sesi�n.
	if ($log != '' || $pass != ''){

	//Miraremos en el fichero xml, en el apartado de usuarios, si se encuentra el
	//usuario pasado como par�metro (por medio del log y pass).
	$root = simplexml_load_file('D2Videos.xml');
	$usuarios = $root->usuarios;
		foreach($usuarios->usuario as $usuario){
			//Si los datos son los correctos, el usuario estar� conectado y 
			//se asignan a las variables session su valores correspondientes
			if ($usuario->nickname == $log && $usuario->contrasena==$pass){
				$_SESSION["conectado"] = 'true'; 
				$_SESSION["idUser"] = (string)$usuario['id'];
				$_SESSION["acierto"] = '1';
				if ($log =='admin' && $pass =='admin'){
					$_SESSION["admin"] = '1';				
				}
				break;
			}else{
			//En caso contrario dejamos vacios todas las variables y la variable session 
			//acierto le damos el valor 2, esta mas abajo ser� la que se encargue de decir
			//que los datos no han sido los correctos.
				$_SESSION["acierto"] = '2';
				$_POST['login']	= '';
				$_POST['pass']	= '';					
			}
 		}
		
	}else{
		$_SESSION["acierto"] = '1';
	}
	
	
?>

<div id = "general">
	<div id = "cabecera">	
		<div id = "home"><a href="index.php"> </a></div>
	
	<?php  	
		//Si la el usuario no esta conectado, se le dar� la posibilidad de hacerlo o de registrarse
		if ($_SESSION["conectado"] == "false") 
			echo ('
			<div align="right">
			<button><a href="#" onclick="muestra_oculta(\'sesion\')" >Iniciar sesi�n</a></button>
			<div id = "sesion">
			<form id = "form1" name="form1" onsubmit="return iniciar_sesion();" enctype="multipart/form-data" method="POST"  action="index.php"  >
				<p>Login: <input id = "login" type = "text" name="login"/></p>
				<p>Password: <input id = "pass" type = "password" name="pass"/></p>
				<input type="submit" name="Submit" value="Aceptar" > <button><a href="#" onclick="muestra_oculta(\'sesion\')" title="">Cerrar</a></button> 
			</form>
			</div>			
			<button><a href="registrarse.html">Registrarse</a></button>
			</div>
			');
		else {//si el usuario ya esta conectado, se le dar� la opci�n de cerrar sesi�n
			echo('
			<div align="right"><button><a href="cerrarSesion.php">Cerrar sesi�n</a></button>
			');
			if ($_SESSION["admin"] == '1'){
				echo('
				<button><a href="actTop6Puntuados.php">Top puntuados</a></button> <button><a href="actTop6Reproducidos.php">Top vistos</a></button>
				');
			}
			echo('</div>');
		}
	?>	
		<div id = "menu">
		 	<div id = "menutop">
				<ul>
					<li><a href="categorias.php?categoria=musica" class="musica">demo</a></li>
					<li><a href="categorias.php?categoria=deportes" class="deportes">demo</a></li>
					<li><a href="categorias.php?categoria=videoJuegos" class="juegos">demo</a></li>
					<li><a href="categorias.php?categoria=social" class="social">demo</a></li>	
					<li><a href="categorias.php?categoria=noticias" class="noticias">demo</a></li>	
				</ul>
			</div>
			<div id = "menubot">
				<ul>
				<?php 	
				if ($_SESSION["conectado"] == "true"){
					echo('<li><button><a href="subidaVideos.php">Subir Videos</a></button></li>');							
				}				
				?>
				<li>Buscador : </li><input type = "text" id="sugerenciasBuscador" onkeypress="buscarVideos(event,this.value);" onkeyup="sugerirVideos(this.value);mostrarBuscador('buscador',1)"/>
				</ul>
				<ul>
					<?php
					if(!empty($_SESSION["conectado"])){
							if($_SESSION["conectado"] != "true"){
								echo('
									<table id="buscador" style="margin-left:193px;">
									</table>
								');
							}else{
								echo('
									<table id="buscador" >
									</table>
								');
							}
						}
				?>
				</ul>			
			</div>
	    </div>
	</div>
	<div id = "contenido">
		<!--La muestra de los ultimos6 videos, como de ambos top6 en realidad son imagenes
			con sus correspondientes t�tulos, las cuales al ser clickeadas nos llevar�n a la 
			p�gina reproductor.php, en la cual se reproducir�n.
			Para ello obtentremos toda la informaci�n necesaria del fichero xml.
			
		-->
		<div id = "videos">
			<br>
			<h2>Ultimos videos subidos</h2>
			<br>

			<?php
			$root = simplexml_load_file('D2Videos.xml');
			$array = array(6);
			$i = 0;	
			$ultimos6 = $root->ultimos6;
			
			foreach($ultimos6->dirVideo as $dirVideo){	
				$array[$i] = $dirVideo;
 				$i = $i + 1;
			}
			$i = 0;				
			echo ('<table>');
			for ($n = 0; $n < 2 ; $n++){
			 echo ('<tr>');
				for ($j = 0; $j < 3 ; $j++){
					echo ('<td>');
					$id = str_replace("video", "", $array[$i]);
 					$video = $root->videos->video[(int)$id - 1];
 					
					echo ('<a href="reproductor.php?videoSeleccionado='. $array[$i] .'">');
					$i = $i + 1;
					echo('<img src="'.$video->fotoPortada.'" height="96" width="170" alt="Miniatura" >');
					echo('<br>');
					echo(substr($video->nombre, 0, 25) .'..');
					echo ('</td>');
				}
			 echo ('</tr>');
			}
			echo ('</table>');
			?>
			<br>
			<h2>Top 6 m�s puntuados</h2>
			<br>
			<?php
			$root = simplexml_load_file('D2Videos.xml');
			$array = array(6);
			$i = 0;	
			$top6Puntuados = $root->top6Puntuados;
			
			foreach($top6Puntuados->dirVideo as $dirVideo){	
				$array[$i] = $dirVideo;
 				$i = $i + 1;
			}
			$i = 0;				
			echo ('<table>');
			for ($n = 0; $n < 2 ; $n++){
			 echo ('<tr>');
				for ($j = 0; $j < 3 ; $j++){
					echo ('<td>');
					$id = str_replace("video", "", $array[$i]);
 					$video = $root->videos->video[(int)$id - 1];
 					
					echo ('<a href="reproductor.php?videoSeleccionado='. $array[$i] .'">');
					$i = $i + 1;
					echo('<img src="'.$video->fotoPortada.'" height="96" width="170" alt="Miniatura" >');
					echo('<br>');
					echo(substr($video->nombre, 0, 25) .'..');
					echo ('</td>');
				}
			 echo ('</tr>');
			}
			echo ('</table>');
			?>
			<br>
			<h2>Top 6 m�s vistos</h2>
			<br>			
			<?php
			$root = simplexml_load_file('D2Videos.xml');
			$array = array(6);
			$i = 0;	
			$top6Reproducidos = $root->top6Reproducidos;
			
			foreach($top6Reproducidos->dirVideo as $dirVideo){	
				$array[$i] = $dirVideo;
 				$i = $i + 1;
			}
			$i = 0;				
			echo ('<table>');
			for ($n = 0; $n < 2 ; $n++){
			 echo ('<tr>');
				for ($j = 0; $j < 3 ; $j++){
					echo ('<td>');
					$id = str_replace("video", "", $array[$i]);
 					$video = $root->videos->video[(int)$id - 1];
 					
					echo ('<a href="reproductor.php?videoSeleccionado='. $array[$i] .'">');
					$i = $i + 1;
					echo('<img src="'.$video->fotoPortada.'" height="96" width="170" alt="Miniatura" >');
					echo('<br>');
					echo(substr($video->nombre, 0, 25) .'..');
					echo ('</td>');
				}
			 echo ('</tr>');
			}
			echo ('</table>');
			?>			
			
			
		</div>	
	
	
	</div>
	<div id="pie"><center >Copyright � 2013 Always Creative. Derechos reservados</center></div>
</div>

<?php 
	if ($_SESSION["acierto"] == '2') {
		echo '<script language="javascript">alert("La usuario o la contrase�a no son los correctos")</script>'; 
	}
?>

</body>




</html>