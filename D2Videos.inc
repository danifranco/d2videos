<?php

function validarFormServ($nombre, $apellidos, $email, $edad, $nick, $pass, $pass2)
{
	$esta = false;
	$error = '';
	// Verificar que los campos obligatorios est�n rellenados
	if($nombre == '')
		$error = $error.'<li>Tu nombre es obligatorio!</li>';
	if($apellidos == '')
		$error = $error.'<li>Tus apellidos son obligatorio!</li>';
	// Verificar el formato de la direcci�n de correo
	if($email != '' && !VerificarFormatoCorreoServ($email))
		$error = $error.'<li>El formato de la direcci�n de correo no es correcto!</li>';
	if($edad == '')
		$error = $error.'<li>Tu edad es obligatoria!</li>';
	if($nick == '')
		$error = $error.'<li>Tu nombre de usuario es obligatorio!</li>';	
	if($pass == '')
		$error = $error.'<li>Tu contrase�a es obligatoria!</li>';
	if($pass2 == '')
		$error = $error.'<li>Repetir tu contrase�a es obligatorio!</li>';
	if($pass != $pass2)
		$error = $error.'<li>Las contrase�as no coinciden!</li>';
		
	$root = simplexml_load_file('D2Videos.xml');
	$usuarios = $root->usuarios;
	
	//buscamos si el nickname del usuario ya existe
	foreach($usuarios->usuario as $usuario){	
		if ($usuario->nickname == $nick )
			$esta = true;
	}
	if($esta == true)
		$error = $error .'<li>Este usuario  ya existe, introduce otro</li>';
	return $error;
	
}
function VerificarFormatoCorreoServ($direccion)
{
	// Asegurar que '@' aparece una �nica vez
	$at=strpos($direccion,'@');
	if($at === false)	//  No se ha encontrado el car�cter '@'
		return false;
	if($at != strrpos($direccion,'@')) // '@' ha aparecido m�s de una vez
		return false;
	// Asegurar que '@' no es el primer caracter
	if($at == 0)
		return false;
	// Asegurar que despu�s de '@' hay, al menos, un punto
	$dot = strrpos($direccion,'.');
	if($dot === false) 	// No se ha encontrado un punto
		return false;
	if($dot < $at)		// No se ha encontrado un punto despu�s de '@'
		return false;
	// Asegurar que despu�s del �ltimo punto hay, al menos, dos caracteres
	if($dot+2 > strlen($direccion) - 1)
		return false;
	return true;
}

function filtro($data)
{
	$data = trim($data); //Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
	$data = stripslashes($data); //Quita las barras de un string con comillas escapadas
	$data = htmlspecialchars($data); // Convierte caracteres especiales en entidades HTML
	return $data;
}

function GuardarUsuarioServ($nombre, $apellidos, $email, $edad, $nick, $pass){
	
	
	$root = simplexml_load_file('D2Videos.xml');
	$numId = $root->usuarios['ult_id'];	 
	$id = $nombre . $numId;
	$nuevoUsuario = $root->usuarios->addChild('usuario');
	$nuevoUsuario['id'] = $id;	
	$nuevoUsuario->addChild('nombre',$nombre);
	$nuevoUsuario->addChild('apellidos',$apellidos);
	$nuevoUsuario->addChild('nickname',$nick);
	$nuevoUsuario->addChild('contrasena',$pass);
	$nuevoUsuario->addChild('edad',$edad);
	$nuevoUsuario->addChild('correo',$email);
	$root->usuarios['ult_id'] = $numId + 1;
	return $root-> asXML('D2Videos.xml');  
	
}
?>