<?php
	$puntuacion = $_GET["puntuacion"];
	$idUsuario = $_GET["idUsuario"];
	$videoSeleccionado = $_GET["video"];
	
	$root = simplexml_load_file('D2Videos.xml');
	$videos = $root->videos;
	
	foreach($videos->video as $video){
		if( $video->attributes()->id == $videoSeleccionado){
			$puntuaciones= $video->puntuaciones;
			//Añadir la nueva puntuación
			$punt = $puntuaciones->addChild('puntuacion');
			$punt->addChild('puntos',$puntuacion);
			$punt->addChild('idUser',$idUsuario);
			
			//Actualizar la puntuación media
			$numeroPuntuaciones = $puntuaciones->attributes()->totalPuntuaciones;
			$sumaPuntuaciones = $puntuaciones->attributes()->sumaPuntuaciones;
			$puntuaciones->attributes()->totalPuntuaciones = $numeroPuntuaciones + 1;
			$puntuaciones->attributes()->sumaPuntuaciones = $sumaPuntuaciones + $puntuacion;
			$video->puntuacionMedia =  ($puntuaciones->attributes()->sumaPuntuaciones)/($puntuaciones->attributes()->totalPuntuaciones);
			echo('   Reproducciones: ' . $video->reproducciones . ' veces	       			      Puntuacion: ' . $video->puntuacionMedia );
		break;
		}
	}
	$root->asXML('D2Videos.xml'); 
	
?>