function muestra_oculta(id){
	if (document.getElementById){ //se obtiene el id
		var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
		el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
	}
}

window.onload = function(){/*hace que se cargue la funci�n lo que predetermina que div estar� oculto hasta llamar a la funci�n nuevamente*/
	muestra_oculta('sesion');/* "sesion" es el nombre que le dimos al DIV */
}

function iniciar_sesion(){
	if (document.form1.login.value == "" ||  document.form1.pass.value  == ""){
		alert("Error, no puede haber campos vacios");
		return false;
	}
	
	return true;
}

//Cambiar las imagenes de las estrellas de color para controlar las puntuaciones
function cambiarImgEstrellas(num,idImagen1,idImagen2,idImagen3,idImagen4,idImagen5){	
		
		if(num == 1){
			document.getElementById(idImagen1).src = "imagenes/estrella.png";
			document.getElementById(idImagen2).src = "imagenes/estrella.png";
			document.getElementById(idImagen3).src = "imagenes/estrella.png";
			document.getElementById(idImagen4).src = "imagenes/estrella.png";
			document.getElementById(idImagen5).src = "imagenes/estrella.png";
		}
		else{
			document.getElementById(idImagen1).src = "imagenes/estrella2.png";
			document.getElementById(idImagen2).src = "imagenes/estrella2.png";
			document.getElementById(idImagen3).src = "imagenes/estrella2.png";
			document.getElementById(idImagen4).src = "imagenes/estrella2.png";
			document.getElementById(idImagen5).src = "imagenes/estrella2.png";
		}		
}

//Comprobaci�n de los datos introducidos en el comentario
function comprobarDatosComentario() {
	if(document.getElementById("tituloComentario").value == ""){
		alert("Por favor escribe un titulo en el comentario");
		return false;
	}
	if(document.getElementById("nuevoComentario").value == ""){
		alert("Por favor escribe un comentario");
		return false;
	}
	
	//Si no hemos tenido ning�n error continuaremos mandandole al servidor el comentario introducido
		var xmlhttp;
		if (window.XMLHttpRequest){
		  // c�digo para IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{
		  // c�digo para IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//Para poder separar las dos informaciones necesarias utilizamos la funci�n split
				data = xmlhttp.responseText.split( "[BRK]" );
				
				//Insertarmos el nuevo comentario en la pagina
				var tr = document.createElement("tr");
				var texto =  
				"<td class=\"coments\">"+
						"<b>" + document.getElementById("tituloComentario").value + "</b>, <font class =\"nombres\">&nbsp;" + data[0] + '</font>,&nbsp;&nbsp;&nbsp;' + data[1] +
						"<br>" +
						"<p id="+">" + document.getElementById("nuevoComentario").value +
						"<button id=\"leerMas\" onclick=\"leerMas(\'' . $comentario['id'] . '\')\">Leer m�s</button> </p>" +
						"</td>";
				tr.innerHTML = texto;
				document.getElementById("tabla").appendChild(tr);
				
				//Insertamos un fila en la tabla para que haya un espacio entre los comentarios
				var tr2 = document.createElement("tr");
				var texto2 = "<td>&nbsp;</td>";
				tr2.innerHTML = texto2;
				document.getElementById("tabla").appendChild(tr2);
				
				//Borramos lo escrito por el comentario antiguo para que tenga la posibilidad de escribir uno nuevo desde 0
				document.getElementById("tituloComentario").value ="";
				document.getElementById("nuevoComentario").value="";
			}
		}
		xmlhttp.open("POST","nuevoComentario.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("tituloComentario=" + document.getElementById("tituloComentario").value + "&nuevoComentario=" + document.getElementById("nuevoComentario").value);
		
}

//Funcion que enviar� al servidor la puntuaci�n elegida por el usuario
function puntuar(video,idUsuario,puntuacion){
	var statusConfirm = confirm("�Realmente deseas puntuar el video?");
    if (statusConfirm == true){	
		var xmlhttp;
		if (window.XMLHttpRequest){
		  // c�digo para IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{
		  // c�digo para IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){
				//Cambiamos las imagen de las estrellas para que se queden en amarillo
				for(var i=1; i < parseInt(puntuacion)+1; i++)
					document.getElementById("estrella"+i).src = "imagenes/estrella.png";
				//Quitar las propiedades de cambio de imagen para que se queden est�ticas las estrellas
				for(i=1; i < 6; i++){
					document.getElementById("estrella"+i).removeAttribute("onmouseout");
					document.getElementById("estrella"+i).removeAttribute("onmouseover");
					document.getElementById("estrella"+i).removeAttribute("onclick");
				}
				document.getElementById("caracteristicasVideo").innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","puntuacion.php?video="+video+"&idUsuario="+idUsuario+"&puntuacion="+puntuacion,true);
		xmlhttp.send();
	}	
}

//Busca los videos que contengan el patr�n especificado
function buscarVideos(e, patron) {
	//Comprobamos si la tecla pulsada es el ENTER
    if (e.keyCode == 13) {
        window.top.location.href ='categorias.php?patron='+ patron;
    }
	
}

//Se encargar� de mostrar o ocultar las sugerencias del buscador
function mostrarBuscador(indentElem, mostrar){
	if(mostrar == 1){
		document.getElementById(indentElem).style.visibility="visible";
	}else{
		document.getElementById(indentElem).style.visibility="hidden";
	}	
}

//Encuentra nombres de videos con un substring dado, 
//funci�n que se utilizar� para mostrar din�micamente nombres de videos sugeridos
function sugerirVideos(str){
	var xmlhttp;
	if (str.length==0){
	  document.getElementById("buscador").innerHTML="";
	  return;
	}
	if (window.XMLHttpRequest){
	  //c�digo for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  //c�digo para IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	  
	    //Reseteamos el buscador
		document.getElementById("buscador").innerHTML = "";
		
		//Para separar los nombres de los videos sugeridos recibidos utilizaremos la funcion split
		data = xmlhttp.responseText.split(" <br> ");
		
		//A�adiremos a las sugerencias del buscador las coincidencias encontradas
		var tr;
		for(i=0; i< data.length;i++){
			tr = document.createElement("tr");
			tr.innerHTML=data[i];
			
			//A�adimos algunos atributos a las filas
			tr.onmouseover= function(){
				this.style.backgroundColor = "#33CCFF";
			}
			tr.onmouseout= function(){
				this.style.backgroundColor = "white";
			}
			tr.onclick= function(){
				var texto = this.innerHTML;
				//Filtramos la etiqueta que usabamos para colorear el texto y los ultimos tres puntos si los tuviera
				var aux1 = texto.replace("<font style=\"color:red;\">" , "");
				var aux2 = aux1.replace("</font>" , "");
				if(aux2.charAt(aux2.length - 1) == '.')
					var resultado = aux2.slice(0,-4);
				else
					var resultado = aux2;
				
				//Insertamos el texto en el buscador
				document.getElementById("sugerenciasBuscador").value = resultado;
				document.getElementById("sugerenciasBuscador").focus();
			}
			
			//Introducimos la nueva fila creada en la tabla
			document.getElementById("buscador").appendChild(tr);
		}

		//Reajustaremos el tama�o de las sugerencias del buscador dependiendo del tama�o de coincidencias encontradas
		document.getElementById("buscador").style.height= (data.lenght*25) + "px";
	  }
	}
	xmlhttp.open("POST","videosSugeridos.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("q=" + str);
}


//validaci�n del formulario

function validar_fomulario(){
	var re = new RegExp(/^[a-z]|\d{3,}@[a-z]{3,}\.[a-z]{2,}$/);
	var re2 = new RegExp(/^\d{3,9}$/);
	var re3 = new RegExp(/^\d{1,2}$/);
	if(document.formRegistro.txtnombre.value == ""){
		alert("Por favor, introduzca su nombre");
		return false;
	}else if(document.formRegistro.txtapellidos.value == ""){
		alert("Por favor, introduzca sus apellidos");
		return false;
	}else if(document.formRegistro.txtcorreo.value == ""){
		alert("Por favor, introduzca su correo");
		return false;
	}else if(document.formRegistro.txttelefono.value == ""){
		alert("Por favor, introduzca su tel�fono");
		return false;			
	}else if(document.formRegistro.txtciudad.value == ""){
		alert("Por favor, introduzca su ciudad");
		return false;	
	}else if(document.formRegistro.txtusuario.value == ""){
		alert("Por favor, introduzca su nombre de usuario");
		return false;			
	}else if(document.formRegistro.txtcontra.value == ""){
		alert("Por favor, introduzca su contrase�a");
		return false;		
	}else if(document.formRegistro.txtcontra2.value == ""){
		alert("Por favor, repita su contrase�a");
		return false;	
	}else if(document.formRegistro.txtedad.value == ""){
		alert("Por favor, introduzca su edad");
		return false;	
	}else if(document.formRegistro.txtcontra.value != document.formRegistro.txtcontra2.value ){
		alert("La contrase�a repetida no coincide.");
		return false;				
	}else if(!re.test(document.formRegistro.txtcorreo.value)){
		alert("El correo introducido no es v�lido");
		return false;
	}else if(!re2.test(document.formRegistro.txttelefono.value)){
		alert("Por favor, proporcione un tel�fono v�lido");
		return false;		
	}else if(!re3.test(document.formRegistro.txtedad.value)){
		alert("Por favor, proporcione una edad v�lida");
		return false;		
	}
	return true;
}

//Muestra el resto del contenido del comentario 'idComentario' del video 'idVideo'
function leerMas(idComentario, idVideo){
	var xmlhttp;
	if (window.XMLHttpRequest){
	  // c�digo para IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  // c�digo para IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById(idComentario).innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("POST","ampliarComentario.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("comentario=" + idComentario +"&video=" + idVideo);	
}

//Aumenta el n�mero de reproducciones del video idVideo y actualiza las reproducciones en la p�gina
function reproduccionTerminada(idVideo){
	var xmlhttp;
	if (window.XMLHttpRequest){
	  // c�digo para IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  // c�digo para IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("caracteristicasVideo").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("POST","sumarReproduccion.php",true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("video="+ idVideo);
}

//validacion subida del video
function validar_subida(){
	if(document.formSubida.txtnombre.value == ""){
		alert("Por favor, introduzca un nombre al v�deo");
		return false;
	}
	else if(document.formSubida.video.value == ""){
		alert("No tienes ningun v�deo seleccionado");
		return false;	
	}
	else if(document.formSubida.video.size > 100000000){
		alert("El tama�o no puede ser superior a 100 MB");
		return false;
	}
	else if(document.formSubida.imagen.value == ""){
		alert("Debe seleccionar una imagen");
		return false;
	}	
	
	return true;
}

//Valida la imagen a subir al servidor
function validarImg(e){
	var imgs = e.target.files; //si tenemos multiples archivos
	var imagen= imgs[0]; // solo nos interesa una.
	var errorHandler = document.querySelector('.error');
	
	if (imagen.size < 500000 ){
		errorHandler.style.opacity = 0;
		var lector = new FileReader();
			
		lector.onload =(function(archivo){
			return function(e){
				document.querySelector('#avatar').src = e.target.result;
			}				
		})(imagen);
			
		lector.readAsDataURL(imagen);
	}else{
		errorHandler.innerHTML = 'Prueba con una imagen m�s peque�a que 500 KB ';
		errorHandler.style.opacity = 1;
	}
}