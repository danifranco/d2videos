<?php

	$root = simplexml_load_file('D2Videos.xml');
	$videos = $root->videos;
	
	//Inicializamos el array del top
	for($i=0; $i < 6; $i++){
		$topPuntuados[$i]= "0.0";
		$dirVideosTop[$i]= "video0";
	}
	
	foreach($videos->video as $video){
	
		$puntuacionMedia = (float) $video->puntuacionMedia;
		
		if($puntuacionMedia > $topPuntuados[0]){
			//Insertar en la 1º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 0, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 0, $video->attributes()->id);
			
		}else if($puntuacionMedia > $topPuntuados[1]){
			//Insertar en la 2º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 1, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 1, $video->attributes()->id);
			
		}else if($puntuacionMedia > $topPuntuados[2]){
			//Insertar en la 3º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 2, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 2, $video->attributes()->id);
			
		}else if($puntuacionMedia > $topPuntuados[3]){
			//Insertar en la 4º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 3, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 3, $video->attributes()->id);
			
		}else if($puntuacionMedia > $topPuntuados[4]){
			//Insertar en la 5º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 4, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 4, $video->attributes()->id);
			
		}else if($puntuacionMedia > $topPuntuados[5]){
			//Insertar en la 6º posición del array
			$topPuntuados = desplazarHaciaAbajo($topPuntuados, 5, $puntuacionMedia);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 5, $video->attributes()->id);
		}
	
	}
	
	//Insertamos en el XML el nuevo top de videos
	for($i=0; $i < 6; $i++){
		$root->top6Puntuados->dirVideo[$i]=$dirVideosTop[$i];
	}
	
	$root->asXML('D2Videos.xml');
	
	
//Desplaza hacia abajo los elementos de un array desde una posición indicada y 
//metiendo como primer elemmento un valor pasado por parámetro
function desplazarHaciaAbajo($array, $pos, $valor){

	//Guardamos el valor dado en una variable auxiliar
	$valorAux= $valor;
	
	for($i=0; $i< 6 - $pos ; $i++){
		//Guardamos el valor viejo
		$aux = $array[$pos+$i];
		//Insertamos el nuevo valor
		$array[$pos+$i] = $valorAux;
		//Actualizamos el valor a meter en la siguiente iteración
		$valorAux = $aux;
	}	
	return $array;		
}

header("Location: index.php");

?>