<?php
session_start();
$tituloComentario = $_POST['tituloComentario'];
$contenidoComent = $_POST['nuevoComentario'];
$videoSeleccionado = $_SESSION["videoSeleccionado"];
$idUser = $_SESSION["idUser"];

$root = simplexml_load_file('D2Videos.xml');
$videos = $root->videos;
		
foreach($videos->video as $video){
	if( $video->attributes()->id == $videoSeleccionado){
		$comentario= $video->comentarios->addChild('comentario');
		$ultimoId = $video->comentarios['ult_id'];
		$comentario->addAttribute('id','coment' . $ultimoId);
		$video->comentarios['ult_id'] = $ultimoId + 1;
		$comentario->addChild('titulo',$tituloComentario);
		$comentario->addChild('idUser',$idUser);
		$comentario->addChild('fecha',date('l jS \of F Y h:i:s A'));
		$comentario->addChild('contenido',$contenidoComent);
		break;
	}
}
//Imprimimos los valores que se necesitarán en el request para formar el coemntario
echo ($comentario->idUser . "[BRK]" . $comentario->fecha);
$root->asXML('D2Videos.xml'); 
	
?>