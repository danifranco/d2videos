<?php

$root = simplexml_load_file('D2Videos.xml');
$videos = $root->videos;

//Coger el patron a buscar
$patron=$_POST["q"];

$resultado ="";
$numVideos=1;

if(strlen($patron) != 0){
	foreach($videos->video as $video){
		//Restricción de mostrar 10 videos únicamente
		if($numVideos == 10) break;
		
		//Comparamos el nombre de los videos para ver si hay patronue mostrarlos o no
		$nombre = strtolower($video->nombre);
		$pos = strpos( strtolower($nombre) , strtolower($patron) );
		if ( $pos !== false){
		
			if(strlen($nombre) > 40){
				//Convertimos el string en un array para luego colorear el subString patronue coincide con el patron dado
				$arr1 = str_split($nombre);
				
				//Coloreamos el patrón introducido
				$trozo ="<font style=\"color:red;\">";
				
				for($i=0;$i < strlen($patron); $i++){
					$trozo = $trozo . $arr1[$pos + $i];
				}
				$trozo = $trozo . "</font>";
				
				//Contamos el número de letras para más adelante mostrar un determinado número de letras únicamente
				$cont = $pos + strlen($trozo);
				$cont = 72 - $cont;
				if($cont < 0) $cont =0;
				
				//Si es la primera vez
				if($numVideos == 1) 
					$resultado= substr($nombre,0,$pos) . $trozo . substr($nombre,$pos + $i,$cont) . " ...";
				else 
					$resultado= $resultado . " <br> " . substr($nombre,0,$pos) . $trozo . substr($nombre,$pos + $i,$cont) . " ...";
			}else{
				$arr1 = str_split($nombre);
				$trozo ="<font style=\"color:red;\">";
				for($i=0; $i < strlen($patron); $i++){
					$trozo = $trozo . $arr1[$pos + $i];
				}
				$trozo = $trozo . "</font>";
				
				//Si es la primera vez
				if($numVideos == 1)
					$resultado= substr($nombre,0,$pos) . $trozo . substr($nombre,$pos + $i,strlen($nombre));
				else
					$resultado= $resultado . " <br> " . substr($nombre,0,$pos) . $trozo . substr($nombre,$pos + $i,strlen($nombre));
			}
			$numVideos++;
		}
	}
}
echo $resultado;

?>