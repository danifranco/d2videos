<?php

	$root = simplexml_load_file('D2Videos.xml');
	$videos = $root->videos;
	
	//Inicializamos el array del top
	for($i=0; $i < 6; $i++){
		$topReproducciones[$i]= "0";
		$dirVideosTop[$i]= "video0";
	}
	
	foreach($videos->video as $video){
	
		$reproducciones = (int) $video->reproducciones;
		
		if($reproducciones > $topReproducciones[0]){
			//Insertar en la 1º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 0, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 0, $video->attributes()->id);
			
		}else if($reproducciones > $topReproducciones[1]){
			//Insertar en la 2º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 1, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 1, $video->attributes()->id);
			
		}else if($reproducciones > $topReproducciones[2]){
			//Insertar en la 3º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 2, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 2, $video->attributes()->id);
			
		}else if($reproducciones > $topReproducciones[3]){
			//Insertar en la 4º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 3, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 3, $video->attributes()->id);
			
		}else if($reproducciones > $topReproducciones[4]){
			//Insertar en la 5º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 4, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 4, $video->attributes()->id);
			
		}else if($reproducciones > $topReproducciones[5]){
			//Insertar en la 6º posición del array
			$topReproducciones = desplazarHaciaAbajo($topReproducciones, 5, $reproducciones);
			$dirVideosTop = desplazarHaciaAbajo($dirVideosTop, 5, $video->attributes()->id);
		}
	
	}
	
	//Insertamos en el XML el nuevo top de videos
	for($i=0; $i < 6; $i++){
		$root->top6Reproducidos->dirVideo[$i]=$dirVideosTop[$i];
	}
	
	$root->asXML('D2Videos.xml');
	
	
//Desplaza hacia abajo los elementos de un array desde una posición indicada y 
//metiendo como primer elemmento un valor pasado por parámetro
function desplazarHaciaAbajo($array, $pos, $valor){

	//Guardamos el valor dado en una variable auxiliar
	$valorAux= $valor;
	
	for($i=0; $i< 6 - $pos ; $i++){
		//Guardamos el valor viejo
		$aux = $array[$pos+$i];
		//Insertamos el nuevo valor
		$array[$pos+$i] = $valorAux;
		//Actualizamos el valor a meter en la siguiente iteración
		$valorAux = $aux;
	}	
	return $array;		
}
header("Location: index.php");

?>