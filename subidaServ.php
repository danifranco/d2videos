<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	<title>Video Subido</title>
		<meta http-equiv="Content-Type" content="text/html; charset= ISO-8859-1" />
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<script type="text/javascript" src="script.js"> </script>
</head>
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<body onclick="mostrarBuscador('buscador',0)">
<?php
	session_start();
	$root = simplexml_load_file('D2Videos.xml');
	$numId = $root->videos['ult_id'];	 
	$idVideo = 'video'. $numId;
	$propietario = $_SESSION["idUser"];
	$nombreVideo = $_POST["txtnombre"];
	$categoria = $_POST["selectCategoria"];
	$fecha = date("F j, Y, g:i a");


	//en caso de que se recargue la página, los datos no volverán a ser subidos
	if ($_SESSION["subir"] == "0"){
		$_SESSION["subir"] = "1";
		//guardar datos
		$nuevoVideo = $root->videos->addChild('video');
		$nuevoVideo['id'] = $idVideo;
		$nuevoVideo->addChild('propietario',$propietario);
		$nuevoVideo->addChild('categoria',$categoria);
		$nuevoVideo->addChild('fecha',$fecha);		
		$nuevoVideo->addChild('direccion','videos/'. $idVideo .'.mp4');
		$nuevoVideo->addChild('fotoPortada','videos/imagenes/'. $idVideo .'.jpg');
		$nuevoVideo->addChild('reproducciones','0');
		$nuevoVideo->addChild('nombre',$nombreVideo);
		$nuevoVideo->addChild('puntuacionMedia','0');
		$nuevoVideo->addChild('comentarios');
		$nuevoVideo->addChild('puntuaciones');
		$root->videos['ult_id'] = $numId + 1;
		
		//buscamos al usuario para asignarle su correspondiente video
		$usuarios = $root->usuarios;
				
		foreach($usuarios->usuario as $usuario){	
				if ($usuario['id'] == $propietario){			
					break;
				}
			}
		$videos = $usuario->videos;
		$videos->addChild('idVideo',$idVideo);

	 
		move_uploaded_file($_FILES['imagen']['tmp_name'],'videos/imagenes/'.$idVideo .'.jpg');
		chmod('videos/imagenes/'.$idVideo .'.jpg',0644);
		
		move_uploaded_file($_FILES['video']['tmp_name'],'videos/'.$idVideo .'.mp4');
		chmod('videos/'.$idVideo .'.mp4',0644);
		
		$array = array(6);
		$i = 0;	
		$ultimos6 = $root->ultimos6;
		
		foreach($ultimos6->dirVideo as $dirVideo){	
			$array[$i] = $dirVideo;
			$i = $i + 1;
		}		
		for ($i = 0; $i < 5; $i++) {
			$array[$i] = $array[$i + 1];			
		}
		$array[5] =  $idVideo;
		$i = 0;	
		
		for ($i = 0; $i < 6; $i++) {
			$root->ultimos6->dirVideo[$i] = $array[$i];	
		}	
		
		$root-> asXML('D2Videos.xml');  		
	}else{
		$idVideo = 'video'. ($numId - 1);
	}
?>
<div id = "general">
	<div id = "cabecera">
	<div id = "home"><a href="index.php"> </a></div>
	<?php  	
		if ($_SESSION["conectado"] == "false") 
			echo ('
			<div align="right">
			<button><a href="#" onclick="muestra_oculta(\'sesion\')" >Iniciar sesión</a></button>
			<div id = "sesion">
			<form id = "form1" name="form1" onsubmit="return iniciar_sesion();" enctype="multipart/form-data" method="POST"  action="index.php"  >
				<p>Login: <input id = "login" type = "text" name="login"/></p>
				<p>Password: <input id = "pass" type = "password" name="pass"/></p>
				<input type="submit" name="Submit" value="Aceptar" > <button><a href="#" onclick="muestra_oculta(\'sesion\')" title="">Cerrar</a></button> 
			</form>	
			</div>
			
			<button><a href="registrarse.html">Registrarse</a></button>
			</div>
			');
		else 
			echo('
			<div align="right"><button><a href="#" onclick="" >Cerrar sesión</a></button></div>
			');

	?>	
		<div id = "menu">
		 	<div id = "menutop">
				<ul>
				<ul>
					<li><a href="categorias.php?categoria=musica" class="musica">demo</a></li>
					<li><a href="categorias.php?categoria=deportes" class="deportes">demo</a></li>
					<li><a href="categorias.php?categoria=videoJuegos" class="juegos">demo</a></li>
					<li><a href="categorias.php?categoria=social" class="social">demo</a></li>	
					<li><a href="categorias.php?categoria=noticias" class="noticias">demo</a></li>		
				</ul>
				</ul>
			</div>
			<div id = "menubot">
				<ul>
				<?php 
					
				if ($_SESSION["conectado"] == "true"){
					echo('<li><button><a href="subidaVideos.php">Subir Videos</a></button></li>');							
				}				
				?>
				<li>Buscador : </li><input type = "text" id="sugerenciasBuscador" onkeypress="buscarVideos(event,this.value);teclas(event)" onkeyup="sugerirVideos(this.value);mostrarBuscador('buscador',1)"/>
				</ul>	
				<ul>
				<?php
				if(!empty($_SESSION["conectado"])){
							if($_SESSION["conectado"] != "true"){
								echo('
									<table id="buscador" style="margin-left:193px;">
									</table>
								');
							}else{
								echo('
									<table id="buscador">
									</table>
								');
							}
						}else{
							echo('
								<table id="buscador" style="margin-left:193px;">
								</table>
							');
						}
				?>
				</ul>	
			</div>
	    </div>
	</div>
	<div id = "contenido">
	 <br>
 

	<center>
	<?php
	
	echo (' 
 		<font size="6"> El video ha sido subido exitosamente: </font><br>
		<video src="'. 'videos/'. $idVideo .'.mp4' . '" poster="' . 'videos/imagenes/'. $idVideo .'.jpg' . '" controls width="640" height="480 "></video><br>
 		');
	?>
	</center>
	</div>
	<div id="pie"><center >Copyright © 2013 Always Creative. Derechos reservados</center></div>
</div>


</body>




</html>