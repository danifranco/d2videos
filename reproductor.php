<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	<title>Reproductor</title>
	
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<script type="text/javascript" src="script.js"> </script>
</head>
<link rel="stylesheet" type="text/css" href="estilo.css"/>
<body onclick="mostrarBuscador('buscador',0)">
<div id = "general">
	<div id = "cabecera">
		<div id = "home"><a href="index.php"> </a></div>
	<?php  	
		session_start();
		if ($_SESSION["conectado"] == "false") 
			echo ('
			<div align="right">
			<button><a href="#" onclick="muestra_oculta(\'sesion\')" >Iniciar sesión</a></button>
			<div id = "sesion">
			<form id = "form1" name="form1" onsubmit="return iniciar_sesion();" enctype="multipart/form-data" method="POST"  action="index.php"  >
				<p>Login: <input id = "login" type = "text" name="login"/></p>
				<p>Password: <input id = "pass" type = "password" name="pass"/></p>
				<input type="submit" name="Submit" value="Aceptar" > <button><a href="#" onclick="muestra_oculta(\'sesion\')" title="">Cerrar</a></button> 
			</form>	
			</div>
			
			<button><a href="registrarse.html">Registrarse</a></button>
			</div>
			');
		else 
			echo('
			<div align="right"><button><a href="cerrarSesion.php">Cerrar sesión</a></button></div>
			
			');

	?>
		<div id = "menu">
			<div id = "menutop">
				<ul>
					<li><a href="categorias.php?categoria=musica" class="musica">demo</a></li>
					<li><a href="categorias.php?categoria=deportes" class="deportes">demo</a></li>
					<li><a href="categorias.php?categoria=videoJuegos" class="juegos">demo</a></li>
					<li><a href="categorias.php?categoria=social" class="social">demo</a></li>	
					<li><a href="categorias.php?categoria=noticias" class="noticias">demo</a></li>	
				</ul>
			</div>
			<div id = "menubot">
				<ul>
					<?php 	
					if(!empty($_SESSION["conectado"])){
						if ($_SESSION["conectado"] == "true"){
							echo('<li><button><a href="subidaVideos.php">Subir Videos</a></button></li>');							
						}	
					}
					?>
					<li>Buscador : </li><input type = "text" id="sugerenciasBuscador" onkeypress="buscarVideos(event,this.value);teclas(event)" onkeyup="sugerirVideos(this.value);mostrarBuscador('buscador',1)"/>
				</ul>
				<ul>
					<?php
						if(!empty($_SESSION["conectado"])){
							if($_SESSION["conectado"] != "true"){
								echo('
									<table id="buscador" style="margin-left:193px;margin-top:-20px">
									</table>
								');
							}else{
								echo('
									<table id="buscador" style="margin-top:-20px">
									</table>
								');
							}
						}else{
							echo('
								<table id="buscador" style="margin-left:193px;margin-top:-20px">
								</table>
							');
						}
	
						echo('<h3 id="nombre">');
						//Coger el video seleccionado
						$videoSeleccionado = $_GET['videoSeleccionado'];
						
						//coger el usuario seleccionado
						if(!empty($_SESSION["idUser"])){ 
							$usuarioConectado = $_SESSION["idUser"];
						}else{
							$usuarioConectado="Anonymous";
						}
						
						$_SESSION["videoSeleccionado"] =$videoSeleccionado;
						
						//Variable que contendra la informacion de si a puntuado o no el usuario
						$_SESSION["puntuado"] = 0;
						$puntuado= $_SESSION["puntuado"];
					
						$root = simplexml_load_file('D2Videos.xml');
						$videos = $root->videos;
						
						foreach($videos->video as $video){
							if( $video['id'] == $videoSeleccionado ){
							echo($video->nombre);
					?>
					</h3>
					
				</ul>	
			</div>
	    </div>
	</div>
	<div id = "contenido">
			
		<?php
				//Continuación del bucle   
					echo('
					<div id = "contenidoSuperior">
					<video id="video" onended="reproduccionTerminada(\''. $video['id'].'\')" src="'. $video->direccion . '" poster="redimensionar.php?imagen='.$video->fotoPortada.'" controls width="640"  ></video><br>
					</div>	
					<div id = "contenidoMedio">
					<ul>');
					//Ajustamos las características del video a las reproducciones
					if($video->reproducciones < 1000000){
						echo('<li><pre id="caracteristicasVideo">   Reproducciones: ' . $video->reproducciones . ' veces	       			          Puntuación: '. substr($video->puntuacionMedia,0,5) .'</pre></li>');
					}else{
						echo('<li><pre id="caracteristicasVideo">   Reproducciones: ' . $video->reproducciones . ' veces	       			Puntuación: '. substr($video->puntuacionMedia,0,5) .'</pre></li>');
					}
					
					echo('
					</ul>
					<ul>		
					<li>
					<div id="puntuacion">');
					$esta = 0;
					//Comprobaremos que hay un usuario conectado
					if($usuarioConectado != "Anonymous"){
						$puntuaciones = $video->puntuaciones;
						if($puntuaciones->count() != 0){
							//Comprobamos si a comentado o no el usuario que está conectado
							foreach($puntuaciones->puntuacion as $puntuacion){
								$idUserPuntuado = $puntuacion->idUser;
								if ( $idUserPuntuado == $usuarioConectado ){
									$esta = 1;
									$puntosElegidos= $puntuacion->puntos;
									break;
								}
							}
						}	
					}
					//Si el usuario que está conectado no ha puntuado se le dará la opción de hacerlo
					if($esta == 0 && $usuarioConectado != "Anonymous"){
						echo('
						<img class= "estrella" id="estrella1" src="imagenes/estrella2.png" 
						onmouseover="cambiarImgEstrellas( 1,\'estrella1\',\'a\', \'a\', \'a\', \'a\')" 
						onmouseout="cambiarImgEstrellas( 0,\'estrella1\',\'a\', \'a\', \'a\', \'a\')" 
						onclick="puntuar(\'' . $videoSeleccionado . '\',\'' . $usuarioConectado . '\',\'1\');"/>
						
						<img class= "estrella" id="estrella2" src="imagenes/estrella2.png" 
						onmouseover="cambiarImgEstrellas( 1,\'estrella1\',\'estrella2\', \'a\', \'a\', \'a\')" 
						onmouseout="cambiarImgEstrellas( 0,\'estrella1\',\'estrella2\', \'a\', \'a\', \'a\')" 
						onclick="puntuar(\'' . $videoSeleccionado . '\',\'' . $usuarioConectado . '\',\'2\');"/>
						
						<img class= "estrella" id="estrella3" src="imagenes/estrella2.png" 
						onmouseover="cambiarImgEstrellas( 1,\'estrella1\',\'estrella2\', \'estrella3\', \'a\', \'a\')" 
						onmouseout="cambiarImgEstrellas( 0,\'estrella1\',\'estrella2\', \'estrella3\', \'a\', \'a\')" 
						onclick="puntuar(\'' . $videoSeleccionado . '\',\'' . $usuarioConectado . '\',\'3\');"/>
						
						<img class= "estrella" id="estrella4" src="imagenes/estrella2.png" 
						onmouseover="cambiarImgEstrellas( 1,\'estrella1\',\'estrella2\', \'estrella3\', \'estrella4\', \'a\')" 
						onmouseout="cambiarImgEstrellas( 0,\'estrella1\',\'estrella2\', \'estrella3\', \'estrella4\', \'a\')" 
						onclick="puntuar(\'' . $videoSeleccionado . '\',\'' . $usuarioConectado . '\',\'4\');"/>
						
						<img class= "estrella" id="estrella5" src="imagenes/estrella2.png" 
						onmouseover="cambiarImgEstrellas( 1,\'estrella1\',\'estrella2\', \'estrella3\', \'estrella4\', \'estrella5\')" 
						onmouseout="cambiarImgEstrellas( 0,\'estrella1\',\'estrella2\', \'estrella3\', \'estrella4\', \'estrella5\')" 
						onclick="puntuar(\'' . $videoSeleccionado . '\',\'' . $usuarioConectado . '\',\'5\');"/>
						');
					//Si el usuario conectado ya ha puntuado el video, se le pondrá su puntuación y no podrá volver a puntuar
					}else{
						//Comprobaremos que hay un usuario registrado para mostrarle su puntuación o no 
						if($usuarioConectado != "Anonymous"){
							for( $i = 1; $i < $puntosElegidos+1; $i++){
								echo('<img class= "estrella" id="estrella' . $i . '" src="imagenes/estrella.png" />');
							}
							for( ; $i < 6; $i++){
								echo('<img class= "estrella" id="estrella' . $i . '" src="imagenes/estrella2.png" />');
							}
						}
					}
					echo('
					</div>
					</li>
					</ul> 
					</div>
					<br><br>
					<div id = "contenidoInferior">
					<table id="tabla">
					');
					if($video->comentarios->count() != 0){
						//Imprimimos los comentarios de los usuarios filtrando cualquier posible caracter especial
						foreach($video->comentarios->comentario as $comentario){	
							echo('<tr>
								  <td class="coments">
							<b>' . filtro($comentario->titulo) . '</b>, <font class ="nombres">&nbsp;' . $comentario->idUser . '</font>,&nbsp;&nbsp;&nbsp;' . $comentario->fecha .
							'<br>');
							//Imprimimos un trozo del comentario únicamente
							$trozo = substr($comentario->contenido,0,300);
							echo('<p id="' . $comentario['id'] . '">' . filtro($trozo) );
							
							//comprobación del tamaño del comentario para ponerle el botón de '+Leer más' o no
							$tamaño = strlen($comentario->contenido);
							if($tamaño <= 300){
								echo('</p>');
							}else{
								echo('<button id="leerMas" onclick="leerMas(\'' . $comentario['id'] . '\' , \'' . $video['id'] . '\')">Leer más</button> </p>');
							}
							
							echo('
							</td>
								  </tr>
								  <tr><td>&nbsp;</td></tr>');
						}
					}
					echo('
					</table>
					<table>
					<tr>
					<td class="coments">
					<pre>Titulo: <input id = "tituloComentario" type = "text" name = "tituloComentario"/>									<button id ="publicarComentario" name="publicarComentario" onclick="comprobarDatosComentario()" >Publicar comentario</button>
					</pre>
					<textarea id="nuevoComentario" name="nuevoComentario"></textarea>
					</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					</table>
					</div>');
					break;
				}	
			}
			
			//Función que filtrará los textos de caracteres raros, especiales o espacios en blanco
			function filtro($data)
			{
				$data = trim($data); //Elimina espacio en blanco (u otro tipo de caracteres) del inicio y el final de la cadena
				$data = stripslashes($data); //Quita las barras de un string con comillas escapadas
				$data = htmlspecialchars($data); // Convierte caracteres especiales en entidades HTML
				return $data;
			}
		?>
	
	</div>
	
	<div id="pie">
	<center >Copyright © 2013 Always Creative. Derechos reservados</center>
	</div>

</div>

</body>

</html>